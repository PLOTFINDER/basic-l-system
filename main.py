import pygame
from pygame.locals import *
import time

from renderer import *

START = "F"
RULE = "F", "F+F-F-F+F"

#### Interesting RULES
# Koch : "F+F-F-F+F"
# Clock : "T-F+T"
# Flower : "T+T-F-T+T+T"
# name_it : "F+T-F-T+F+T"


START_POS = 0, 600
POS_INCREMENT = 5, 5


def applyRule(path):
    path = path.replace(RULE[0], RULE[1])
    return path


def simulate():
    WINDOW_SIZE = (1200, 600)

    pygame.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    path = START

    mouse_pos = None
    pause = True

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("Courbe de Koch")
    mainClock = pygame.time.Clock()

    simulation = True

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    pass

        if not pause:
            pass

        if frameCount % 60 == 0 and not pause:
            path = applyRule(path)
            render(window, path, POS_INCREMENT, START_POS)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()
