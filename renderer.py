import pygame


def render(window, path, pos_increment=(5, 5), start_pos=(600, 300)):
    clearWindow(window)

    directions = [(1, 0), (0, 1), (-1, 0), (0, -1)]

    direction = 0

    pos = start_pos

    print()

    color = (255, 255, 255)

    for command in path:
        if command == "+":
            direction -= 1
        elif command == "-":
            direction += 1
        elif command == "F":
            increment = directions[direction][0] * pos_increment[0], directions[direction][1] * pos_increment[1]
            end_pos = pos[0] + increment[0], pos[1] + increment[1]
            pygame.draw.line(window, color, pos, end_pos)
            pos = end_pos

        direction = direction % 4




def clearWindow(window):
    window.fill((0, 0, 0))
